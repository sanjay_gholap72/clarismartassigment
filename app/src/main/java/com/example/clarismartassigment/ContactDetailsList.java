package com.example.clarismartassigment;

import java.util.List;

public class ContactDetailsList
{
    String name;
    String phone;


    public ContactDetailsList(String name, String phone)
    {
        this.name = name;
        this.phone = phone;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}
