package com.example.clarismartassigment;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.ContentResolver;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;


public class Home_Activity extends AppCompatActivity
{
    private RecyclerView recyclerView;
    private List<ContactDetailsList> bContactDetailsList;
    private String name,phone_Number;
    ContactsAdapter contactsAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        recyclerView = (RecyclerView) findViewById(R.id.id_recyclerview);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        bContactDetailsList = new ArrayList<ContactDetailsList>();
        contactsAdapter = new ContactsAdapter(this,bContactDetailsList);
        recyclerView.setAdapter(contactsAdapter);
        LoadContactDetails();

        recyclerView.setAdapter(contactsAdapter);

    }

    private void LoadContactDetails()
    {
        StringBuilder stringBuilder = new StringBuilder();
        ContentResolver contentResolver = getContentResolver();
        Cursor cursor = contentResolver.query(ContactsContract.Contacts.CONTENT_URI,null,null,null,null);

        if(cursor.getCount()>0)
        {
            while (cursor.moveToNext())
            {

                String id = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts._ID));
                name = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
                int phoneNumber = Integer.parseInt(cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER)));

                if (phoneNumber > 0)
                {
                    Cursor phoneCursor = contentResolver.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " =? ", new String[]{id}, null);
                    while (phoneCursor.moveToNext())
                    {
                         phone_Number = phoneCursor.getString(phoneCursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                        Log.wtf("name", name);
                        Log.wtf("phone number", phone_Number);

                        bContactDetailsList.add(new ContactDetailsList(name,phone_Number));
                    }
                    phoneCursor.close();
                }
            }
        }cursor.close();
    }
}
