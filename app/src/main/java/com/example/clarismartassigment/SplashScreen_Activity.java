package com.example.clarismartassigment;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.widget.ImageView;
import android.widget.TextView;

public class SplashScreen_Activity extends AppCompatActivity
{
    private ImageView logo;
    private TextView businessName;
    private static int splashTimeOut=5000;
    private static int SPLASH_TIME_OUT = 5000;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splashscreen);

        logo=(ImageView)findViewById(R.id.id_imglogo);

        new Handler().postDelayed(new Runnable()
        {

            @Override
            public void run()
            {

                Intent i = new Intent(SplashScreen_Activity.this, Home_Activity.class);
                startActivity(i);

                // close this activity
                finish();
            }
        }, SPLASH_TIME_OUT);

    }
}
