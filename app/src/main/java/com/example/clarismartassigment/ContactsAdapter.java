package com.example.clarismartassigment;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import org.w3c.dom.Text;

import java.util.List;

public class ContactsAdapter extends  RecyclerView.Adapter<ContactsAdapter.ContactViewHolder>
{
    private Context mContext;
    String name,phone;
    private List<ContactDetailsList> mDataSet;

    public ContactsAdapter(Context mContext, List<ContactDetailsList> mDataSet)
    {
        this.mContext = mContext;
        this.mDataSet = mDataSet;
    }

    @NonNull
    @Override
    public ContactViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        View v;
        v = LayoutInflater.from(parent.getContext()).inflate(R.layout.contact_list,parent, false);
        return new ContactViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ContactViewHolder holder, int position)
    {
        ContactDetailsList contactDetailsList = mDataSet.get(position);
        name = contactDetailsList.getName();
        phone = contactDetailsList.getPhone();

        holder.txtName.setText(name);
        holder.txtPhone.setText(phone);

    }

    @Override
    public int getItemCount()
    {
        return mDataSet.size();
    }

    public class ContactViewHolder extends RecyclerView.ViewHolder
    {
        TextView txtName,txtPhone;

        public ContactViewHolder(@NonNull View itemView)
        {
            super(itemView);

            txtName = itemView.findViewById(R.id.id_name);
            txtPhone = itemView.findViewById(R.id.id_phone);
        }
    }

}
